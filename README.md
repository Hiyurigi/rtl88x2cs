# :zap: **RTL8822CS** :zap:
An RTL8822CS driver with fixes for mainline kernels

## :information_source: **How to build** :information_source:

### **Native**
```bash
git clone https://codeberg.org/Hiyurigi/rtl88x2cs
cd rtl88x2cs
make ARCH=your-architecture
#if you using clang do:
make ARCH=your-architecture CC=clang
```

### **CROSS COMPILE**
```bash
git clone https://codeberg.org/Hiyurigi/rtl88x2cs
cd rtl88x2cs
make ARCH=your-architecture CROSS_COMPILE=your-toolcahain-path KSRC=kernel-headers-path
#if you using clang do:
make ARCH=your-architecture CROSS_COMPILE=your-toolcahain-path KSRC=kernel-headers-path CC=clang
```

### **Example**
```bash
git clone https://codeberg.org/Hiyurigi/rtl88x2cs
cd rtl88x2cs
make ARCH=arm64 CC=clang
#CROSS COMPILE
make ARCH=arm64 CROSS_COMPILE=~/toolchain/bin/aarch64-linux-gnu- KSRC=~/linux-6.0 CC=clang
```

<mark>You can do **make strip** to strip the module file before installing the driver.</mark>

## :information_source: **How to install** :information_source:

Simply type:
```bash
make install
```